package controllers

import (
	"MARCOGPT/configs"
	db "MARCOGPT/db"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/go-playground/validator"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var questionnCollection *mongo.Collection = configs.GetCollection(configs.DB, "questions")
var validate = validator.New()

func CompleteCode(w http.ResponseWriter, r *http.Request) {
	var code db.Code
	var codeQuestion db.CodeQuestion
	var output db.Output
	var mongoDb db.MongoDBQeustions
	client := &http.Client{}
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Ask a proper question")
	}
	json.Unmarshal(reqBody, &codeQuestion)

	code.Prompt = codeQuestion.CodeQuestion
	code.Model = "code-davinci-002"

	code.MaxTokens = 2000
	reqBody1, err := json.Marshal(&code)

	if err != nil {
		log.Println(err)
	}
	var jsonStr = []byte(reqBody1)
	mongoDb.Question = string(reqBody1)
	id := uuid.New()
	mongoDb.Id = id.String()
	req1, _ := http.NewRequest("POST", "https://api.openai.com/v1/completions", bytes.NewBuffer(jsonStr))
	req1.Header.Set("Content-Type", "application/json")
	req1.Header.Set("Authorization", "Bearer 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")

	resp1, err := client.Do(req1)
	if err != nil {
		log.Println(err)
	}
	if resp1.StatusCode != 200 {
		fmt.Println(resp1.Status)
	}
	defer resp1.Body.Close()
	body1, err := ioutil.ReadAll(resp1.Body)

	if err := json.Unmarshal(body1, &output); err != nil {
		fmt.Println("error unmarshalling:", err)
	}
	fmt.Fprintf(w, "response:"+output.Choices[0].Text)
	mongoDb.Answer = output.Choices[0].Text
	result, err := questionnCollection.InsertOne(nil, mongoDb)
	if result != nil {
		w.WriteHeader(http.StatusCreated)
		fmt.Fprintf(w, "\n \n result added to mongoDB")
	} else {
		fmt.Fprintf(w, err.Error())
	}
}

func AskChatGPT(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	w.Header().Set("Content-Type", "application/json")
	var question db.Question
	var mongoDb db.MongoDBQeustions
	client := &http.Client{}
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Ask a proper question")
		return
	}
	json.Unmarshal(reqBody, &question)
	var input db.Input
	var output db.Output
	input.Prompt = question.Question
	input.Model = "text-davinci-003"
	input.N = 2
	input.MaxTokens = 1024
	reqBody1, err := json.Marshal(&input)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(string(reqBody1))
	mongoDb.Question = string(reqBody1)
	id := uuid.New()
	mongoDb.Id = id.String()
	var jsonStr = []byte(reqBody1)
	req1, _ := http.NewRequest("POST", "https://api.openai.com/v1/completions", bytes.NewBuffer(jsonStr))
	req1.Header.Set("Content-Type", "application/json")
	req1.Header.Set("Authorization", "Bearer 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")
	resp1, err := client.Do(req1)
	if err != nil {
		log.Println(err)
		return
	}
	if resp1.StatusCode != 200 {
		fmt.Println(resp1.Status)
		return
	}
	defer resp1.Body.Close()
	body1, err := ioutil.ReadAll(resp1.Body)
	if err != nil {
		log.Println(err)
		return
	}
	if err := json.Unmarshal(body1, &output); err != nil {
		fmt.Println("error unmarshalling:", err)
		return
	}

	mongoDb.Answer = output.Choices[0].Text
	result, err := questionnCollection.InsertOne(nil, mongoDb)
	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}
	if result == nil {
		fmt.Fprintf(w, "result not added to mongoDB")
		return
	}

	str := output.Choices[0].Text
	str1 := strings.Trim(str, "\n")
	response1 := map[string]string{"message": str1}
	json.NewEncoder(w).Encode(response1)
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	response := map[string]string{"message": "I am alive!"}
	json.NewEncoder(w).Encode(response)
}

func ReadMongoDBData(w http.ResponseWriter, r *http.Request) {
	cursor, err := questionnCollection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.TODO())
	for cursor.Next(context.TODO()) {
		var question bson.M
		if err = cursor.Decode(&question); err != nil {
			log.Fatal(err)
		}
		s := fmt.Sprint(question)
		fmt.Fprintf(w, s)
	}
}

func DeleteMongoDBData(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	fmt.Println("id", id)
	result, err := questionnCollection.DeleteOne(context.TODO(), bson.M{"id": id})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "DeleteOne removed %v document(s)\n", result.DeletedCount)
}

func DropMongoDBDatabase(w http.ResponseWriter, r *http.Request) {
	if err := questionnCollection.Drop(context.TODO()); err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "Database Dropped")
}
