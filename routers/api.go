package routers

import (
	"MARCOGPT/controllers"

	"github.com/gorilla/mux"
)

func UserRoute(router *mux.Router) {
	//ADMIN
	router.HandleFunc("/health", controllers.HealthCheck)

	//MONGO DB
	router.HandleFunc("/getMongoData", controllers.ReadMongoDBData)
	router.HandleFunc("/deleteMongoData/{id}", controllers.DeleteMongoDBData)
	router.HandleFunc("/dropMongoDatabase", controllers.DropMongoDBDatabase)

	//MarcoGPT
	// router.HandleFunc("/question", func(w http.ResponseWriter, r *http.Request) {
	// 	controllers.AskChatGPT(w, r)
	// }).Methods("POST")
	router.HandleFunc("/question", controllers.AskChatGPT).Methods("POST", "OPTIONS")
	router.HandleFunc("/code", controllers.CompleteCode).Methods("POST")
}
