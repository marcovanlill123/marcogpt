package controllers

type Input struct {
	Model       string `json:"model"`
	Prompt      string `json:"prompt"`
	MaxTokens   int    `json:"max_tokens"`
	Temperature int    `json:"temperature"`
	TopP        int    `json:"top_p"`
	N           int    `json:"n"`
	Stream      bool   `json:"stream"`
	Stop        string `json:"stop"`
}
type Choices []struct {
	Text         string `json:"text"`
	Index        int    `json:"index"`
	LogProbs     string `json:"log_probs"`
	FinishReason string `json:"finish_reason"`
}
type Usage struct {
	PromptToken      int `json:"prompt_tokens"`
	CompletionTokens int `json:"completion_tokens"`
	TotalTokens      int `json:"total_tokens"`
}
type Output struct {
	ID      string `json:"id"`
	Object  string `json:"object"`
	Created int    `json:"created"`
	Model   string `json:"model"`
	Choices Choices
	Usage   Usage
}

type Question struct {
	Question string `json:"question"`
}

type CodeQuestion struct {
	CodeQuestion string `json:"code_question"`
}

type Code struct {
	Model            string `json:"model"`
	Prompt           string `json:"prompt"`
	MaxTokens        int    `json:"max_tokens"`
	Temperature      int    `json:"temperature"`
	TopP             int    `json:"top_p"`
	FerquencyPenalty int    `json:"frequency_penalty"`
	PresencePenalty  int    `json:"presence_penalty"`
}

type MongoDBQeustions struct {
	Id string `json:"id"`
	Question string `json:"question"`
	Answer   string `json:"answer"`
}
