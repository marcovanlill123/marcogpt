package main

import (
	"MARCOGPT/configs"
	"MARCOGPT/routers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)

	//run database
	configs.ConnectDB()
	routers.UserRoute(router) //add this

	log.Fatal(http.ListenAndServe(":8090", router))
}
